#!/bin/bash
startYear='1924'

cat header.txt > "yritykset_$startYear.csv"

for f in $(ls companies_$startYear-*.txt); do 
    cat $f >> "yritykset_$startYear.csv" 
    printf "\n" >> "yritykset_$startYear.csv" 
done
