import axios from "axios";
import fs from "fs";

// http://avoindata.prh.fi/ytj.html#!/bis47v1/search
// const url = "http://avoindata.prh.fi/tr/v1?totalResults=true&companyRegistrationFrom=1972-01-01&companyRegistrationTo=1972-12-31";

const sendGetRequest = async (url,startDate,endDate, maxResults) => {
    try {
        
        // get data
        const resp = await axios.get(url);
        

        console.log(`For time period ${startDate} to ${endDate}, got ${resp.data.results.length} total results.` );
        if (resp.data.results.length.length > maxResults) {
            console.log(`WARNING: this exceeds maxResults ${maxResults}. Some results may have been skipped.`);
        }

        // filter out undesired company forms
        const reqCompForms = ["AOY","OYJ","OY"];
        const filteredData = resp.data.results.filter(  (element) => reqCompForms.includes(element.companyForm));

        // put results in a JSON
        const results = filteredData.map( (element) => {

            // const date = new Date(element.registrationDate);
            //const dateString = date.toLocaleDateString("fi-FI");
            const dateString = element.registrationDate;

            const newResult = {
                "Y-tunnus" : element.businessId,
                "Yritysmuoto" : element.companyForm,
                "Nimi" : element.name,
                "Rekisteröintipäivämäärä" : dateString,
            };

            return newResult;
        });

        //
        console.log(`${results.length} records will be written.` );

        // convert JSON to a suitable CSV format
        const replacer = (key, value) => value === null ? "" : value; // specify how you want to handle null values here
        const header = Object.keys(results[0]);
        const csv = [
            // header.join(","), // header row first
            ...results.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(","))
        ].join("\n");

        const fileName = "companies" + "_" + startDate +"_" + endDate + ".txt"; 

        fs.writeFile(fileName, csv, (err) => {
            if (err)
                console.log(err);
            else {
                console.log(`File ${fileName} written successfully\n`);
            }
        });


    } catch (err) {
        // Handle Error Here
        console.error(err);  // prints whole failure
        // console.error("Search failed.");
    }
};


function getDaysInMonth(m, y) {
    return m===2 ? y & 3 || !(y%25) && y & 15 ? 28 : 29 : 30 + (m+(m>>3)&1);
}

const startYear = "1973";
for (let i=1; i<=12;i+=3) {
    const startMonth = i.toString().padStart(2,"0");
    const endMonth = (i+2).toString().padStart(2,"0");
    const endDay = getDaysInMonth(i+2,Number(startYear));
    
    const startDate = startYear + "-" + startMonth +"-01";
    const endDate =  startYear + "-" + endMonth +"-" + endDay;

    const url = "http://avoindata.prh.fi/tr/v1?maxResults=1000&companyRegistrationFrom=" + startDate + "&companyRegistrationTo=" +endDate;
    sendGetRequest(url,startDate,endDate,1000);
}

